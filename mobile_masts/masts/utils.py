import datetime
import csv
import itertools
from operator import itemgetter

from django.core.files import File
from django.utils import timezone

from masts.models import Csv, Company, Mast, MastLease

expected_headers = ["Property Name", "Property Address [1]", "Property  Address [2]",
                    "Property Address [3]", "Property Address [4]", "Unit Name",
                    "Tenant Name", "Lease Start Date", "Lease End Date", "Lease Years",
                    "Current Rent"]


def get_leases_by_current_rent(csv_file, show_five=None):
    masts_data = csv.DictReader(csv_file)
    leases = []
    if masts_data.fieldnames is None:
        return []

    if masts_data.fieldnames != expected_headers:
        return [{'message': 'Wrong, missing or extra headers'}]

    for linea in masts_data:
        linea['Current Rent'] = float(linea['Current Rent'])
        leases.append(linea)

    if not leases:
        return []

    leases_data_ordered = sorted(leases, key=itemgetter('Current Rent'))

    if show_five:
        return leases_data_ordered[:5]
    return leases_data_ordered


def get_leases_of_twenty_five_years(csv_file, total_rent=None):
    masts_data = csv.DictReader(csv_file)

    if masts_data.fieldnames is None:
        return []

    if masts_data.fieldnames != expected_headers:
        return [{'message': 'Wrong, missing or extra headers'}]

    result = [x for x in masts_data if int(x['Lease Years']) == 25]

    if total_rent:
        total = 0
        for x in result:
            total += float(x['Current Rent'])
        return total

    if not result:
        return [{'message': 'Could not find any lease with "Lease Years" = 25'}]

    return result


def get_leases_by_range_date(csv_file):
    # Get leases between 1st June 1999 and 31st August 2007

    masts_data = csv.DictReader(csv_file)
    leases = []

    if masts_data.fieldnames is None:
        return []

    if masts_data.fieldnames != expected_headers:
        return [{'message': 'Wrong, missing or extra headers'}]

    for linea in masts_data:
        lease_start_date = datetime.datetime.strptime(linea['Lease Start Date'], '%d %b %Y')
        lease_end_date = datetime.datetime.strptime(linea['Lease End Date'], '%d %b %Y')
        linea['Lease Start Date'] = lease_start_date.strftime('%d/%m/%Y')
        linea['Lease End Date'] = lease_end_date.strftime('%d/%m/%Y')

        if 1999 < lease_start_date.year < 2007 or \
                (lease_start_date.year == 2007 and lease_start_date.month < 9) or \
                lease_start_date.year == 1999 and lease_start_date.month > 5:
            leases.append(linea)
    if not leases:
        leases = [{'message': 'Could no find any lease between that date range'}]

    return leases


def get_tenant_masts_count(csv_file):
    masts_data = csv.DictReader(csv_file)
    tenant = {}

    if masts_data.fieldnames is None:
        return {'error_message': 'the csv file is empty'}

    if masts_data.fieldnames != expected_headers:
        return {'error_message': 'Wrong, missing or extra headers'}

    for linea in masts_data:
        try:
            tenant[linea['Tenant Name']] += 1
        except KeyError:
            tenant[linea['Tenant Name']] = 1

    if not tenant:
        return {'error_message': 'the csv file could be empty or has missing values'}

    return tenant


############################################################

class CSVImportError(Exception):
    def __init__(self, message=""):
        self.message = message


def create_csv(csv_file):
    csv_model = Csv()
    csv_model.name = 'masts_content'
    csv_model.csv_file.save('masts_leases', (File(csv_file)))
    csv_model.save()


    return csv_model


def create_company(tenant_name):
    # company = Company()
    # company.name = tenant_name
    # company.save()
    company = Company.objects.get_or_create(name=tenant_name)[0]
    return company


def create_masts(unit_name, company):
    # mast = Mast()
    # mast.name = unit_name
    # mast.tenant = company
    # mast.save()
    mast = Mast.objects.get_or_create(name=unit_name, tenant=company)[0]
    return mast


def import_csv(csv_file, csv_instance=None):
    reader_image = csv.DictReader(csv_file)
    iter_dict = [reader_image] * 10000

    if reader_image.fieldnames is None:
        error_message = 'Empty file.'
        raise CSVImportError(error_message)

    if reader_image.fieldnames != expected_headers:
        error_message = 'wrong headers.'
        raise CSVImportError(error_message)

    lease_batch = []
    for batch in itertools.zip_longest(*iter_dict):
        for lines in batch:
            if lines is None:
                break

            if lines['Property Name'] == '' or lines['Unit Name'] == '' or \
                    lines['Lease Start Date'] == '' or lines['Lease End Date'] == '' or \
                    lines['Lease Years'] == '' or lines['Current Rent'] == '':
                error_message = 'missing some mandatory values, check the csv'
                raise CSVImportError(error_message)

            company = create_company(lines['Tenant Name'])
            mast = create_masts(lines['Unit Name'], company)

            lease_start_date = datetime.datetime.strptime(lines['Lease Start Date'], '%d %b %Y')
            lease_end_date = datetime.datetime.strptime(lines['Lease End Date'], '%d %b %Y')
            lines['Lease Start Date'] = lease_start_date.strftime('%Y-%m-%d')
            lines['Lease End Date'] = lease_end_date.strftime('%Y-%m-%d')

            mast_lease = MastLease()
            mast_lease.property_name = lines['Property Name']
            mast_lease.property_address_1 = lines['Property Address [1]']
            mast_lease.property_address_2 = lines['Property  Address [2]']
            mast_lease.property_address_3 = lines['Property Address [3]']
            mast_lease.property_address_4 = lines['Property Address [4]']
            mast_lease.unit_name = mast
            mast_lease.tenant_name = company
            mast_lease.start_date = lines['Lease Start Date']
            mast_lease.end_date = lines['Lease End Date']
            mast_lease.lease_years = lines['Lease Years']
            mast_lease.current_rent = lines['Current Rent']
            mast_lease.csv = csv_instance

            lease_batch.append(mast_lease)
        MastLease.objects.bulk_create(lease_batch)
        lease_batch = []
