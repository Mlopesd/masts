from django.shortcuts import render

from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticatedOrReadOnly

from masts.models import Company, MastLease, Mast
from masts.serializers import CompanySerializer, MastLeaseSerializer, MastSerializer


class CompanyViewSet(viewsets.ModelViewSet):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer

    permission_classes = (IsAuthenticatedOrReadOnly,)


class MastViewSet(viewsets.ModelViewSet):
    queryset = Mast.objects.all()
    serializer_class = MastSerializer

    permission_classes = (IsAuthenticatedOrReadOnly,)


class MastLeaseViewSet(viewsets.ModelViewSet):
    queryset = MastLease.objects.all()
    serializer_class = MastLeaseSerializer

    permission_classes = (IsAuthenticatedOrReadOnly,)
