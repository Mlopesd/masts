from django.contrib import admin

from masts.models import Csv, Company, Mast, MastLease


class CsvAdmin(admin.ModelAdmin):
    list_display = ['name', 'date']


class MastInline(admin.TabularInline):
    model = Mast

    def has_change_permission(self, request, obj=None):
        return False


class CompanyAdmin(admin.ModelAdmin):
    inlines = [MastInline]
    list_display = ['name', 'total_masts_rented']

    def total_masts_rented(self, obj):
        return obj.masts_lease.count()


class MastAdmin(admin.ModelAdmin):
    list_display = ['name', 'tenant']


class MastLeaseAdmin(admin.ModelAdmin):
    list_display = ['tenant_name', 'property_name', 'start_date', 'end_date', 'lease_years',
                    'current_rent']


admin.site.register(Csv, CsvAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(Mast, MastAdmin)
admin.site.register(MastLease, MastLeaseAdmin)
