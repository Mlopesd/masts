from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone


class Csv(models.Model):
    name = models.CharField(max_length=30)
    csv_file = models.FileField()
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f'{self.name}'


class Company(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Mast(models.Model):
    name = models.CharField(max_length=200)
    tenant = models.ForeignKey(Company, on_delete=models.SET_NULL, null=True, blank=True,
                               related_name='masts', verbose_name='Tenant')

    def __str__(self):
        return self.name


class MastLease(models.Model):
    property_name = models.CharField(max_length=30)
    property_address_1 = models.CharField(max_length=140, null=True, blank=True)
    property_address_2 = models.CharField(max_length=140, null=True, blank=True)
    property_address_3 = models.CharField(max_length=140, null=True, blank=True)
    property_address_4 = models.CharField(max_length=140, null=True, blank=True)
    unit_name = models.ForeignKey(Mast, on_delete=models.SET_NULL, null=True, blank=True,
                                  related_name='lease', verbose_name='Unit name')
    tenant_name = models.ForeignKey(Company, on_delete=models.SET_NULL, null=True, blank=True,
                                    related_name='masts_lease', verbose_name='Tenant name')
    start_date = models.DateField(verbose_name='Lease start date')
    end_date = models.DateField(verbose_name='Lease end date')
    lease_years = models.IntegerField(verbose_name='Lease years')
    current_rent = models.FloatField(verbose_name='Rent')
    csv = models.ForeignKey(Csv, on_delete=models.SET_NULL, null=True, blank=True)
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.tenant_name}, {self.property_name}'
