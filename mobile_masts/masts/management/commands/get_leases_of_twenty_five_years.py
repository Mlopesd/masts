import argparse

from django.core.management.base import BaseCommand

from masts.utils import get_leases_of_twenty_five_years


class Command(BaseCommand):
    help = 'Show a list of mast data with “Lease Years” = 25 years'

    def add_arguments(self, parser):

        parser.add_argument(
            'csv_file',
            type=argparse.FileType('r'),
            default='Python Developer Test Dataset.csv',
            nargs='?',
            help='csv file dataset. if not provided then '
                 '"Python Developer Test Dataset.csv" will be the default')

        parser.add_argument(
            '-t', '--total_rent', action='store_true', help='Show the total rent in the list.')

    def handle(self, *args, **kwargs):
        total_rent = kwargs['total_rent']
        csv_file = kwargs['csv_file']

        leases = get_leases_of_twenty_five_years(csv_file, total_rent)
        if total_rent:
            if leases and leases != 0:
                self.stdout.write('total rent: ' + str(leases))
            else:
                self.stdout.write('Could not find any lease with "Lease Years" = 25')
        elif leases:
            for masts_data in leases:
                for x in masts_data.values():
                    if x != '':
                        self.stdout.write(str(x), ending=', ')
                self.stdout.write(ending='\n\n')
        else:
            message = 'the csv file could be empty or has missing values'
            self.stdout.write(message)
