import argparse

from django.core.management.base import BaseCommand

from masts.utils import get_leases_by_current_rent


class Command(BaseCommand):
    help = 'Produce a list sorted by “Current Rent” in ascending order'

    def add_arguments(self, parser):

        parser.add_argument(
            'csv_file',
            type=argparse.FileType('r'),
            default='Python Developer Test Dataset.csv',
            nargs='?',
            help='csv file dataset. if not provided then '
                 '"Python Developer Test Dataset.csv" will be the default')

        parser.add_argument(
            '-f', '--five', action='store_true', help='Show first five in the list.')

    def handle(self, *args, **kwargs):
        show_five = kwargs['five']
        csv_file = kwargs['csv_file']

        leases = get_leases_by_current_rent(csv_file, show_five)

        if leases:
            for masts_data in leases:
                for x in masts_data.values():
                    if x != '':
                        self.stdout.write(str(x), ending=', ')
                self.stdout.write(ending='\n\n')
        else:
            message = 'the csv file is empty'
            self.stdout.write(message)
