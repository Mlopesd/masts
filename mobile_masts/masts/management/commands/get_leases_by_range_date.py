import argparse

from django.core.management.base import BaseCommand

from masts.utils import get_leases_by_range_date


class Command(BaseCommand):
    help = 'List the data for rentals with “Lease Start Date”' \
           ' between 1st June 1999 and 31st August 2007'

    def add_arguments(self, parser):
        parser.add_argument(
            'csv_file',
            type=argparse.FileType('r'),
            default='Python Developer Test Dataset.csv',
            nargs='?',
            help='csv file dataset. if not provided then '
                 '"Python Developer Test Dataset.csv" will be the default')

    def handle(self, *args, **kwargs):
        csv_file = kwargs['csv_file']

        leases = get_leases_by_range_date(csv_file)
        if leases:
            for masts_data in leases:
                for x in masts_data.values():
                    if x != '':
                        self.stdout.write(str(x), ending=', ')
                self.stdout.write(ending='\n\n')
        else:
            message = 'the csv file could be empty or has missing values'
            self.stdout.write(message)
