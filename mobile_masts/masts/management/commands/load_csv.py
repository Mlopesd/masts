import argparse
import sys

from django.core.management.base import BaseCommand
from django.db import transaction

from masts.utils import create_csv, CSVImportError, import_csv


class Command(BaseCommand):
    help = 'It will load the DB with the csv content'

    def add_arguments(self, parser):
        parser.add_argument(
            'csv_file',
            type=argparse.FileType('r'),
            help='csv file name, where the mast data is contained')

    @transaction.atomic
    def handle(self, *args, **kwargs):
        csv_file = kwargs['csv_file']

        try:
            csv_instance = create_csv(csv_file)
            csv_file.seek(0)
            import_csv(csv_file, csv_instance=csv_instance)
        except CSVImportError as exc:
            self.stdout.write(exc.message)
            sys.exit(1)
