import argparse

from django.core.management.base import BaseCommand

from masts.utils import get_leases_by_range_date, get_tenant_masts_count


class Command(BaseCommand):
    help = 'Create a dictionary containing tenant name and' \
           ' a count of masts for each tenant'

    def add_arguments(self, parser):
        parser.add_argument(
            'csv_file',
            type=argparse.FileType('r'),
            default='Python Developer Test Dataset.csv',
            nargs='?',
            help='csv file dataset. if not provided then '
                 '"Python Developer Test Dataset.csv" will be the default')

    def handle(self, *args, **kwargs):
        csv_file = kwargs['csv_file']

        leases = get_tenant_masts_count(csv_file)

        if leases and 'error_message' not in leases:
            for tenant, count in leases.items():
                if tenant != '':
                    self.stdout.write(f'{tenant} has ----> #{str(count)} masts')
        else:
            message = leases['error_message'] or 'the csv file could be empty or has missing values'
            self.stdout.write(message)
