from django.apps import AppConfig


class MastsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'masts'
