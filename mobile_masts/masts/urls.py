from django.urls import include, path
from rest_framework.routers import DefaultRouter

from masts import views

router = DefaultRouter()
router.register(r'companies', views.CompanyViewSet)
router.register(r'masts', views.MastViewSet)
router.register(r'mastleases', views.MastLeaseViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
]

app_name = 'masts'
