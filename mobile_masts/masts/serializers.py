from rest_framework import serializers

from masts.models import Company, Mast, MastLease


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ['name']


class MastSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mast
        fields = ['name']


class MastLeaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = MastLease
        fields = '__all__'
