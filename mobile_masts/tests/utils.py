import os


def path_maker(file):
    path = os.path.join(os.path.dirname(__file__), "masts/management_commands/csvs/" + file)
    return path


expected_ordered_list_first_five = \
    'Potternewton Crescent, Potternewton Est Playing Field, LS7, Potternewton Est Playing Field, Arqiva Ltd, 24 Jun 1999, 23 Jun 2019, 20, 6600.0, \n\n' \
    'Queenswood Heights, Queenswood Heights, Queenswood Gardens, Headingley, Leeds, Queenswood Hgt-Telecom App., Vodafone Ltd, 08 Nov 2004, 07 Nov 2029, 25, 9500.0, \n\n' \
    'Armley - Burnsall Grange, Armley, LS13, Burnsall Grange CSR 37865, O2 (UK) Ltd, 26 Jul 2007, 25 Jul 2032, 25, 12000.0, \n\n' \
    'Seacroft Gate (Chase) - Block 2, Telecomms Apparatus, Leeds, LS14, Seacroft Gate (Chase) block 2-Telecom App., Vodafone Ltd., 30 Jan 2004, 29 Jan 2029, 25, 12250.0, \n\n' \
    'Seacroft Gate (Chase) - Block 2, Telecomms Apparatus, Leeds, LS14, Seacroft Gate (Chase) - Block 2, WYK 0414, Hutchinson3G Uk Ltd&Everything Everywhere Ltd, 21 Aug 2007, 20 Aug 2032, 25, 12750.0, \n\n'

expected_tenants_masts_count = \
    'Arqiva Services ltd has ----> #1 masts\n' \
    'Arqiva Ltd has ----> #1 masts\n' \
    'Vodafone Ltd. has ----> #1 masts\n' \
    'Vodafone Ltd has ----> #1 masts\n' \
    'O2 (UK) Ltd has ----> #1 masts\n' \
    'Hutchinson3G Uk Ltd&Everything Everywhere Ltd has ----> #1 masts\n' \
    'Everything Everywhere Ltd has ----> #4 masts\n' \
    'Everything Everywhere Ltd & Hutchinson 3G UK has ----> #3 masts\n' \
    'EverythingEverywhere Ltd & Hutchinson3GUK Ltd has ----> #1 masts\n' \
    'Everything Everywhere Ltd&Hutchison 3G UK Ltd has ----> #8 masts\n' \
    'Everything Everywhere Ltd&Hutchison 3G UK LTd has ----> #1 masts\n' \
    'Everything Everywhere Ltd&Hutchsion 3G UK Ltd has ----> #1 masts\n' \
    'Everything Everywhere Ltd&Hutchsion 3G Ltd has ----> #1 masts\n' \
    'Everything Everywhere Ltd & Hutchison 3G Ltd has ----> #1 masts\n' \
    'Cornerstone Telecommunications Infrastructure has ----> #16 masts\n'
