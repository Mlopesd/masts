from io import StringIO

import pytest
from django.core.management import call_command, CommandError

from tests.utils import path_maker


class TestGetLeasesOfTwentyFiveYears:

    def test_with_default_csv(self):
        out = StringIO()
        call_command('get_leases_of_twenty_five_years', stdout=out)

        assert '30 Jan 2004, 29 Jan 2029, 25,' in out.getvalue()
        assert '08 Nov 2004, 07 Nov 2029, 25,' in out.getvalue()
        assert '26 Jul 2007, 25 Jul 2032, 25,' in out.getvalue()
        assert '21 Aug 2007, 20 Aug 2032, 25,' in out.getvalue()

    def test_with_extra_csv(self):
        # the csv file was defined as extra argument when calling the management command
        out = StringIO()
        call_command('get_leases_of_twenty_five_years',
                     path_maker('masts_dataset.csv'), stdout=out)

        assert '30 Jan 2004, 29 Jan 2029, 25,' in out.getvalue()
        assert '08 Nov 2004, 07 Nov 2029, 25,' in out.getvalue()

    def test_no_lease_found_between_date_range(self):
        out = StringIO()
        message_expected = 'Could not find any lease with "Lease Years" = 25, \n\n'
        call_command('get_leases_of_twenty_five_years',
                     path_maker('no_lease_between_date_range.csv'), stdout=out)

        assert out.getvalue() == message_expected

    def test_total_rent(self):
        # calling the management command with the optional arg --total_rent
        out = StringIO()
        call_command('get_leases_of_twenty_five_years',
                     path_maker('masts_dataset.csv'), '--total_rent', stdout=out)

        assert 'total rent: 21750.0\n' in out.getvalue()

    def test_total_rent_with_empty_csv(self):
        # calling the management command with the optional arg --total_rent
        out = StringIO()
        call_command('get_leases_of_twenty_five_years',
                     path_maker('empty.csv'), '--total_rent', stdout=out)

        assert 'Could not find any lease with "Lease Years" = 25' in out.getvalue()

    #
    # --------------- csv reading cases --------------- #
    #
    def test_empty_file(self):
        out = StringIO()
        message_expected = 'the csv file could be empty or has missing values\n'

        call_command('get_leases_of_twenty_five_years', path_maker('empty.csv'), stdout=out)

        assert out.getvalue() == message_expected

    def test_csv_not_found(self):
        with pytest.raises(CommandError) as pytest_wrapped_e:
            out = StringIO()
            call_command('get_leases_of_twenty_five_years', path_maker('doesnt_exist.csv'),
                         stdout=out)

        assert 'No such file or directory' in str(pytest_wrapped_e.value)
        assert out.getvalue() == ''

    def test_csv_with_just_headers(self):
        out = StringIO()
        message_expected = 'Could not find any lease with "Lease Years" = 25, \n\n'

        call_command('get_leases_of_twenty_five_years', path_maker('just_headers.csv'), stdout=out)

        assert out.getvalue() == message_expected

    def test_csv_with_wrong_headers(self):
        out = StringIO()
        message_expected = 'Wrong, missing or extra headers, \n\n'

        call_command('get_leases_of_twenty_five_years', path_maker('wrong_headers.csv'), stdout=out)

        assert out.getvalue() == message_expected

    def test_csv_with_missing_headers(self):
        out = StringIO()
        message_expected = 'Wrong, missing or extra headers, \n\n'

        call_command('get_leases_of_twenty_five_years', path_maker('missing_header.csv'),
                     stdout=out)

        assert out.getvalue() == message_expected

    def test_csv_with_extra_headers(self):
        out = StringIO()
        message_expected = 'Wrong, missing or extra headers, \n\n'

        call_command('get_leases_of_twenty_five_years', path_maker('more_headers.csv'), stdout=out)

        assert out.getvalue() == message_expected
