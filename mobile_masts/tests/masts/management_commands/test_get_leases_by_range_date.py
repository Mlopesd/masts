from io import StringIO

import pytest
from django.core.management import call_command, CommandError

from tests.utils import path_maker


class TestGetLeasesByRangeDate:
    def test_with_default_csv(self):
        out = StringIO()
        call_command('get_leases_by_range_date', stdout=out)

        assert '24/06/1999' in out.getvalue()
        assert '30/01/2004' in out.getvalue()
        assert '08/11/2004' in out.getvalue()
        assert '26/07/2007' in out.getvalue()
        assert '21/08/2007' in out.getvalue()

    def test_with_extra_csv(self):
        # the csv file was defined as extra argument when calling the management command
        out = StringIO()
        call_command('get_leases_by_range_date',
                     path_maker('masts_dataset.csv'), stdout=out)

        assert '24/06/1999' in out.getvalue()
        assert '30/01/2004' in out.getvalue()
        assert '08/11/2004' in out.getvalue()

    def test_no_lease_found_between_date_range(self):
        out = StringIO()
        message_expected = 'Could no find any lease between that date range, \n\n'
        call_command('get_leases_by_range_date',
                     path_maker('no_lease_between_date_range.csv'), stdout=out)

        assert out.getvalue() == message_expected

    #
    # --------------- csv reading cases --------------- #
    #
    def test_empty_file(self):
        out = StringIO()
        message_expected = 'the csv file could be empty or has missing values\n'

        call_command('get_leases_by_range_date', path_maker('empty.csv'), stdout=out)

        assert out.getvalue() == message_expected

    def test_csv_not_found(self):
        with pytest.raises(CommandError) as pytest_wrapped_e:
            out = StringIO()
            call_command('get_leases_by_range_date', path_maker('doesnt_exist.csv'), stdout=out)

        assert 'No such file or directory' in str(pytest_wrapped_e.value)
        assert out.getvalue() == ''

    def test_csv_with_just_headers(self):
        out = StringIO()
        message_expected = 'Could no find any lease between that date range, \n\n'

        call_command('get_leases_by_range_date', path_maker('just_headers.csv'), stdout=out)

        assert out.getvalue() == message_expected

    def test_csv_with_wrong_headers(self):
        out = StringIO()
        message_expected = 'Wrong, missing or extra headers, \n\n'

        call_command('get_leases_by_range_date', path_maker('wrong_headers.csv'), stdout=out)

        assert out.getvalue() == message_expected

    def test_csv_with_missing_headers(self):
        out = StringIO()
        message_expected = 'Wrong, missing or extra headers, \n\n'

        call_command('get_leases_by_range_date', path_maker('missing_header.csv'), stdout=out)

        assert out.getvalue() == message_expected

    def test_csv_with_extra_headers(self):
        out = StringIO()
        message_expected = 'Wrong, missing or extra headers, \n\n'

        call_command('get_leases_by_range_date', path_maker('more_headers.csv'), stdout=out)

        assert out.getvalue() == message_expected
