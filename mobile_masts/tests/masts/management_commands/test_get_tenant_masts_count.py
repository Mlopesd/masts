from io import StringIO

import pytest
from django.core.management import call_command, CommandError

from tests.utils import path_maker, expected_tenants_masts_count


class TestGetLeasesByCurrentRen:
    def test_with_default_csv(self):
        out = StringIO()
        call_command('get_tenant_masts_count', stdout=out)

        assert expected_tenants_masts_count == out.getvalue()

    def test_with_extra_csv(self):
        # the csv file was defined as extra argument when calling the management command
        out = StringIO()
        call_command('get_tenant_masts_count',
                     path_maker('Python Developer Test Dataset.csv'), stdout=out)

        assert expected_tenants_masts_count in out.getvalue()

    #
    # --------------- csv reading cases --------------- #
    #
    def test_empty_file(self):
        out = StringIO()
        message_expected = 'the csv file is empty\n'

        call_command('get_tenant_masts_count', path_maker('empty.csv'), stdout=out)

        assert out.getvalue() == message_expected

    def test_csv_not_found(self):
        with pytest.raises(CommandError) as pytest_wrapped_e:
            out = StringIO()
            call_command('get_tenant_masts_count', path_maker('doesnt_exist.csv'), stdout=out)

        assert 'No such file or directory' in str(pytest_wrapped_e.value)
        assert out.getvalue() == ''

    def test_csv_with_just_headers(self):
        out = StringIO()
        message_expected = 'the csv file could be empty or has missing values\n'

        call_command('get_tenant_masts_count', path_maker('just_headers.csv'), stdout=out)

        assert out.getvalue() == message_expected

    def test_csv_with_wrong_headers(self):
        out = StringIO()
        message_expected = 'Wrong, missing or extra headers\n'

        call_command('get_tenant_masts_count', path_maker('wrong_headers.csv'), stdout=out)

        assert out.getvalue() == message_expected

    def test_csv_with_missing_headers(self):
        out = StringIO()
        message_expected = 'Wrong, missing or extra headers\n'

        call_command('get_tenant_masts_count', path_maker('missing_header.csv'), stdout=out)

        assert out.getvalue() == message_expected

    def test_csv_with_extra_headers(self):
        out = StringIO()
        message_expected = 'Wrong, missing or extra headers\n'

        call_command('get_tenant_masts_count', path_maker('more_headers.csv'), stdout=out)

        assert out.getvalue() == message_expected
