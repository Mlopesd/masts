from io import StringIO

import pytest
from django.core.management import call_command, CommandError

from tests.utils import path_maker, expected_ordered_list_first_five


class TestGetLeasesByCurrentRen:
    def test_with_default_csv(self):
        out = StringIO()
        call_command('get_leases_by_current_rent', '--five', stdout=out)

        assert expected_ordered_list_first_five == out.getvalue()

    def test_first_five_current_rent(self):
        # calling the management command with the optional arg --five
        out = StringIO()
        call_command('get_leases_by_current_rent',
                     'Python Developer Test Dataset.csv', '--five', stdout=out)

        assert expected_ordered_list_first_five == out.getvalue()

    def test_with_extra_csv(self):
        # the csv file was defined as extra argument when calling the management command
        out = StringIO()
        call_command('get_leases_by_current_rent',
                     path_maker('Python Developer Test Dataset.csv'), stdout=out)

        assert expected_ordered_list_first_five in out.getvalue()

    #
    # --------------- csv reading cases --------------- #
    #
    def test_empty_file(self):
        out = StringIO()
        message_expected = 'the csv file is empty\n'

        call_command('get_leases_by_current_rent', path_maker('empty.csv'), stdout=out)

        assert out.getvalue() == message_expected

    def test_csv_not_found(self):
        with pytest.raises(CommandError) as pytest_wrapped_e:
            out = StringIO()
            call_command('get_leases_by_current_rent', path_maker('doesnt_exist.csv'), stdout=out)

        assert 'No such file or directory' in str(pytest_wrapped_e.value)
        assert out.getvalue() == ''

    def test_csv_with_just_headers(self):
        out = StringIO()
        message_expected = 'the csv file is empty\n'

        call_command('get_leases_by_current_rent', path_maker('just_headers.csv'), stdout=out)

        assert out.getvalue() == message_expected

    def test_csv_with_wrong_headers(self):
        out = StringIO()
        message_expected = 'Wrong, missing or extra headers, \n\n'

        call_command('get_leases_by_current_rent', path_maker('wrong_headers.csv'), stdout=out)

        assert out.getvalue() == message_expected

    def test_csv_with_missing_headers(self):
        out = StringIO()
        message_expected = 'Wrong, missing or extra headers, \n\n'

        call_command('get_leases_by_current_rent', path_maker('missing_header.csv'), stdout=out)

        assert out.getvalue() == message_expected

    def test_csv_with_extra_headers(self):
        out = StringIO()
        message_expected = 'Wrong, missing or extra headers, \n\n'

        call_command('get_leases_by_current_rent', path_maker('more_headers.csv'), stdout=out)

        assert out.getvalue() == message_expected
