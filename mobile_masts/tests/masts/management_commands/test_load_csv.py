from io import StringIO

import pytest
from django.core.management import call_command, CommandError

from masts.models import MastLease, Mast, Company, Csv
from tests.utils import path_maker


@pytest.mark.django_db
class TestLoadImages:

    def test_csv_with_wrong_headers(self):
        out = StringIO()
        message_expected = 'missing some mandatory values, check the csv\n'

        with pytest.raises(SystemExit):
            call_command('load_csv', path_maker('missing_values.csv'), stdout=out)

        assert message_expected == out.getvalue()
        assert MastLease.objects.count() == 0
        assert Mast.objects.count() == 0
        assert Company.objects.count() == 0
        assert Csv.objects.count() == 0

    def test_empty_file(self):
        out = StringIO()
        with pytest.raises(SystemExit):
            call_command('load_csv', path_maker('empty.csv'), stdout=out)

        assert out.getvalue() == 'Empty file.\n'

    def test_csv_not_found(self):
        with pytest.raises(CommandError) as pytest_wrapped_e:
            out = StringIO()
            call_command('load_csv', path_maker('doesnt_exist.csv'), stdout=out)

        assert 'No such file or directory' in str(pytest_wrapped_e.value)
        assert out.getvalue() == ''

    def test_csv_with_wrong_headers(self):
        out = StringIO()
        message_expected = 'wrong headers.\n'

        with pytest.raises(SystemExit):
            call_command('load_csv', path_maker('wrong_headers.csv'), stdout=out)

        assert message_expected == out.getvalue()
        assert MastLease.objects.count() == 0
        assert Mast.objects.count() == 0
        assert Company.objects.count() == 0
        assert Csv.objects.count() == 0

    def test_csv_with_missing_headers(self):
        out = StringIO()
        message_expected = 'Wrong, missing or extra headers, \n\n'

        with pytest.raises(SystemExit):
            call_command('load_csv', path_maker('missing_header.csv'), stdout=out)

        print("\n\nel msj:\n------", out.getvalue(), "----\n\n")
        assert MastLease.objects.count() == 0
        assert Mast.objects.count() == 0
        assert Company.objects.count() == 0
        assert Csv.objects.count() == 0

    def test_csv_with_extra_headers(self):
        out = StringIO()
        message_expected = 'wrong headers.\n'

        with pytest.raises(SystemExit):
            call_command('load_csv', path_maker('more_headers.csv'), stdout=out)

        assert out.getvalue() == message_expected
        assert MastLease.objects.count() == 0
        assert Mast.objects.count() == 0
        assert Company.objects.count() == 0
        assert Csv.objects.count() == 0
