# Mobile Masts lease project

## Setup

   1. Create a virtual environment and activate it
   2. Install requirements: `pip install -r requirements.txt`

# Management commands
Being in mobile_masts directory run

    `$ python manage.py --help`
you will be able to see all the options available under the
`[masts]` sections.

### for the assignment 1:

    `$ python manage.py get_leases_by_current_rent`
    or
    `$ python manage.py get_leases_by_current_rent --five`

to just get the first five on the list

### Assignment 2: 

    `$ python manage.py get_leases_of_twenty_five_years`
    or
    `$ python manage.py get_leases_of_twenty_five_years --total_rent`
    
to get the sum of the total rent on the list

### Assignment 3:

    `$ python manage.py get_leases_by_range_date`

### Assignment 4:

    `$ python manage.py get_tenant_masts_count`

You can do --help on each management commands to get the arguments and
optional arguments of the commands

#### On each command you can specify a csv file like this:

    `$ python manage.py get_leases_by_current_rent csv_file_example.csv`

#### if not provided then "Python Developer Test Dataset.csv" will be the default.
#
## Management command to load csv on DB.
    `$ python manage.py load_csv example_csv_file.csv`
#

# Admin site or API
- Run a local server: `python manage.py runserver`

Rest-API:
- http://127.0.0.1:8000/masts/api/

Admin-site: 
- http://127.0.0.1:8000/admin/

Login with superuser credentials:
- username: `admin`
- password: `x`


# Testing
- Run pytest to run all the tests in the project

`$ pytest`
or `$ pytest tests/`

- To run a specific test file:

- `$ pytest test_example.py`
 or `$ pytest tests/masts/management_commands/test_example.py`